#include<stdio.h>
#include<math.h>
#include<string.h>

struct position{
    char name[20];
    double x1;
    double y1;
};

double distance(double x1, double y1, double x2, double y2)
{
    double x_sq = (x1-x2)*(x1-x2);
    double y_sq = (y1-y2)*(y1-y2);
    return sqrt(x_sq + y_sq);
}

int main()
{
    while(1){
        char str1[20], str2[20];
        double x1, y1;
        double a1, b1;

        scanf("%s %lf%lf", str1, &a1, &b1);

        scanf("%s %lf%lf", str2, &x1, &y1);
        struct position pointA;
        struct position pointB;

        strcpy(pointA.name, str1);
        pointA.x1 = a1;
        pointA.y1 = b1;

        strcpy(pointB.name, str2);
        pointB.x1 = x1;
        pointB.y1 = y1;

        double dist = distance(pointA.x1, pointA.y1, pointB.x1, pointB.y1);

        printf("Distance between %s to %s: %.3lf\n", str1, str2, dist);

        //printf("%1lf", pointA.x1);
    }

}
