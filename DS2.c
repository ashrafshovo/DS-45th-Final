#include<stdio.h>
int main()
{
    while(1){
    int n, i;
    scanf("%d", &n);
    double ara[n], odd_avg =0, odd_sum =0, even_avg =0, even_sum =0;
    double count_even = 0, count_odd = 0;

    for(i=0;i<n;i++){
        scanf("%lf", &ara[i]);
    }

    for(i=0;i<n;i++){
        if((int)ara[i] % 2 == 0){
            even_sum += ara[i];
            count_even++;
        }
        else {
            odd_sum +=ara[i];
            count_odd++;
        }
    }

    odd_avg = odd_sum / count_odd;
    even_avg = even_sum / count_even;

    printf("%.1lf %.1lf\n", odd_avg, even_avg);

    }
}
